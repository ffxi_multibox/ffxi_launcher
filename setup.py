from setuptools import setup, find_packages
import re, io

__version__ = re.search(
    r'__version__\s*=\s*[\'"]([^\'"]*)[\'"]',  # It excludes inline comment too
    io.open('ffxi/__init__.py', encoding='utf_8_sig').read()
    ).group(1)

def parse_requirements(requirements):
    with open(requirements, 'r') as f:
        return [
            s for s in [
                line.split('#', 1)[0].strip(' \t\n') for line in f
            ] if s != ''
        ]

reqs = parse_requirements('requirements.txt')

package_name = 'ffxi'

setup(
    name='ffxi-launcher',
    version=__version__,
    author='Bambooya',
    packages=find_packages(include=[package_name]),
    install_requires=reqs,
    entry_points={
        'gui_scripts': [f'ffxi-launcher={package_name}.launcher:main'],
        'console_scripts': [f'ffxi-pol={package_name}.pol:main'],
    },
    package_data={
        package_name: [f'images/*.png']
    },
    python_requires='>=3.7,<4',
)
