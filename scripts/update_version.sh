#!/bin/sh

VERSION="$1"
sed -i "s/^__version__\s*=\s*'[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+'/__version__ = '${VERSION}'/" ffxi/__init__.py
# sed -i "s/ffxi_launcher-[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+-py3-none-any.whl/ffxi_launcher-${VERSION}-py3-none-any.whl/" README.md
