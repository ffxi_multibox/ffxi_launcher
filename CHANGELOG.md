## [1.0.5](https://gitlab.com/ffxi_multibox/ffxi_launcher/compare/v1.0.4...v1.0.5) (2023-04-26)


### Bug Fixes

* simplify window update ([e9d146e](https://gitlab.com/ffxi_multibox/ffxi_launcher/commit/e9d146ea8167f12b8cbfe5d7044805dbafdc4cde))

## [1.0.4](https://gitlab.com/ffxi_multibox/ffxi_launcher/compare/v1.0.3...v1.0.4) (2023-04-15)


### Bug Fixes

* serialize waiting for pol and ffxi windows to appear ([e943a4c](https://gitlab.com/ffxi_multibox/ffxi_launcher/commit/e943a4ce571f799957b7d553a33135e003880108))

## [1.0.3](https://gitlab.com/ffxi_multibox/ffxi_launcher/compare/v1.0.2...v1.0.3) (2023-03-24)


### Bug Fixes

* handle comments in ashita .ini file ([5d6b3c0](https://gitlab.com/ffxi_multibox/ffxi_launcher/commit/5d6b3c0f29f4cbcd269a62482e0a047659e470b3))

## [1.0.2](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/compare/v1.0.1...v1.0.2) (2022-10-03)


### Bug Fixes

* resolve issue where launcher timeout occurs.  resolve separate issue where launcher detects the wrong process with a window title that is a superstring of the desired window title. ([b5e0374](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/b5e0374a1042f643e9f8a34ef6c6e725428954ad))

## [1.0.1](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/compare/v1.0.0...v1.0.1) (2022-10-01)


### Bug Fixes

* minor adjustments to timing to reduce errors during login process ([a091f41](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/a091f4134a228b5cf19721c083cb443b3bfbffe8))

# 1.0.0 (2022-09-01)


### Bug Fixes

* add hotkey support to pause/resume and stop the launcher in progress ([19f5c2b](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/19f5c2b0a683ddcef8d25d19999c98932c355f69))
* Add support for laupol launching independent of windows dpi setting ([667ee93](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/667ee932f7ff66cb0029bf061073cb7bc1d2a9b0))
* change mouse movement from instantaneous to as fast as possible to help mitigate failed clicks in pol ([24799ce](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/24799ce342ff62052e0b4e13025240a24719390d))
* cicd pipeline running on release tag ([8246eb4](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/8246eb4d15c5c6a4f5a7ac0403b3b795856e0d1e))
* cicd pipeline running on release tag ([1afc812](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/1afc8120b3ddad4c0fb72bce26d6dd92aef7bd06))
* cicd pipeline running on release tag ([eae2943](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/eae29430f6902ca06ab65293b35f66fcfffa417d))
* corrected offset in virtual keyboard overlay ([ba9991a](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/ba9991af81d769c6f2ed197ecaaef018358c0819))
* improve error handling and logging ([1a576e6](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/1a576e6bc4f7e0700e3b2086efb162be292a727e))
* normalize line endings ([012261c](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/012261cc9d42c038a6a2a747d3534929317a051a))
* Speed improved when launching ashita by starting processes concurrently. ([c498d8a](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/c498d8a31dd4a1ce786650d2b9f8ab9c4bfdae87))
* test semantic release ([b03d2ff](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/b03d2ffb5303e2de41a80a9524f15e24f2161d29))
* test semantic release ([6a74a35](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/6a74a35e0835ed00039348c94f9e40e5f64ab092))
* test semantic release ([de9858d](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/de9858d8628a10386e0b4b954bd973c4078f066b))
* test semantic release ([f45f961](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/f45f9619d5284df8c55f29459feec493b4cb7253))
* update semantic release ([a6158d7](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/a6158d7c2295878b6b48ea4548ef77f0a0f9b442))
* update semantic release ([69122e9](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/69122e94c5208e841293e8621584926a772471b3))
* update semantic release ([e75ffdf](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/e75ffdf7aa6f09954d41dcab8fc82d714bf6f276))
* update semantic release ([450d4b4](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/450d4b47900a2f2fc0de174f40f80fc8b96a041b))
* update semantic release ([ded9d8e](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/ded9d8eacb75f3f6096c992a4ac7738b9f9c3c8d))


### Features

* The launcher now supports both windower and ashita. ([8ae82a5](https://gitlab.com/ffxi_multibox/ffxi_multibox_launcher/commit/8ae82a5690d4215240f108ff7896865569adf4ee))

## [0.1.2](https://gitlab.com/bambooya/ffxi_multibox_launcher/compare/v0.1.1...v0.1.2) (2022-08-24)


### Bug Fixes

* Add support for laupol launching independent of windows dpi setting ([2f354c4](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/2f354c4c5976e902e879d902a7d45d62e5928cc3))

## [0.1.1](https://gitlab.com/bambooya/ffxi_multibox_launcher/compare/v0.1.0...v0.1.1) (2022-08-19)


### Bug Fixes

* Speed improved when launching ashita by starting processes concurrently. ([6bbc944](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/6bbc944d435dc66258d6c9442b68334385d21dc3))

# [0.1.0](https://gitlab.com/bambooya/ffxi_multibox_launcher/compare/v0.0.10...v0.1.0) (2022-08-18)


### Features

* The launcher now supports both windower and ashita. ([efee6bb](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/efee6bbf8aee532b881a56fda928c16aa1e007f6))

## [0.0.10](https://gitlab.com/bambooya/ffxi_multibox_launcher/compare/v0.0.9...v0.0.10) (2022-08-17)


### Bug Fixes

* corrected offset in virtual keyboard overlay ([a842810](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/a84281026e34336f154aff6813266f09e0be9341))
* improve error handling and logging ([c218d07](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/c218d07c51d2bc3f3457af11b70a9136b1f50593))
* Merge branch 'fix' into 'master' ([1b1df1f](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/1b1df1f5f1e569a28a01619155c449ce1793cb45))

## [0.0.9](https://gitlab.com/bambooya/ffxi_multibox_launcher/compare/v0.0.8...v0.0.9) (2022-08-16)


### Bug Fixes

* change mouse movement from instantaneous to as fast as possible to help mitigate failed clicks in pol ([a7e5a4f](https://gitlab.com/bambooya/ffxi_multibox_launcher/commit/a7e5a4f1a04a26835b214c13063410fd1c3dade5))
