- [Overview](#overview)
- [Prerequisites](#prerequisites)
- [How to set up and launch your team](#how-to-set-up-and-launch-your-team)
- [How to manage passwords](#how-to-manage-passwords)
- [How to launch more than four accounts](#how-to-launch-more-than-four-accounts)
- [Problems and solutions](#problems-and-solutions)
  - [PlayOnline accounts are corrupted](#playonline-accounts-are-corrupted)
  - [The launcher stops clicking upon reaching the ffxi "accept" dialog](#the-launcher-stops-clicking-upon-reaching-the-ffxi-accept-dialog)

# Overview

This repo contains python scripts to automate launching multiple instances of [Windower4](http://windower.net/) to enable multiboxing Final Fantasy XI.  Written and tested originally on a 1920x1080 display and currently on a 3840x2160 display and may require modifications for other resolutions (not sure on this).

# Prerequisites
1. Install python 3.7-3.10 from the Microsoft Store.  Other python distributions (e.g., conda) should work as well, but are beyond the scope of this readme.
2. Download the ffxi-launcher python wheel file from the [gitlab package registry](https://gitlab.com/bambooya/ffxi_multibox_launcher/-/packages/).
    1. Click the `ffxi-launcher` link corresponding to the desired version.
    2. Scroll all the way to the bottom of the next page, ignoring the instructions about setting up a personal access token. Download the latest wheel file by clicking the `ffxi_launcher-x.x.x-py3-none-any.whl` link under "Files".
3. Install the ffxi-launcher python wheel by double clicking the wheel file downloaded in the previous step.  Alternatively, the wheel file can be installed from the command line:
    ```
    pip install ffxi_launcher-*-py3-none-any.whl
    ```
4. Verify the install by running the following from the command line and confirming that the `ffxi-launcher` package with the correct version appears in the output:
    ```
    pip list
    ```

# How to set up and launch your team

1. Set up all of your accounts in PlayOnline.  It is currently necessary to save the pol password for each account you want to launch.  Optionally, if you want to be able to launch more than four accounts, see [How to launch more than four accounts](#how-to-launch-more-than-four-accounts).

2. Follow instructions in [How to manage passwords](#how-to-manage-passwords) to enable logging in with your Square Enix password.  This is currently required.

3. Configure which characters you want to launch by creating or modifying the existing `ffxi_launcher_config.yaml` file.  Example [ffxi_launcher_config.yaml](https://gitlab.com/kicksalva/ffxi_multibox_launcher/blob/main/ffxi_launcher_config.yaml) file is provided and included below for reference:

    ```yaml
    team:
      - name: Hugatree
        pol_account_idx: 0
        ffxi_character_idx: 3
        profile: US_4k
        pol_keyboard_sequence: pol_keyboard_sequence
      - name: Bambooya
        pol_account_idx: 1
        ffxi_character_idx: 0
        profile: US_4k
        pol_keyboard_sequence: pol_keyboard_sequence
      - name: Deeejay
        pol_account_idx: 2
        ffxi_character_idx: 0
        profile: US_4k
        pol_keyboard_sequence: pol_keyboard_sequence
      - name: Dazzledor
        pol_account_idx: 3
        ffxi_character_idx: 0
        profile: US_4k
        pol_keyboard_sequence: pol_keyboard_sequence
      - name: Weetata
        pol_account_idx: 0
        ffxi_character_idx: 1
        profile: EU_4k
        pol_keyboard_sequence: pol_keyboard_sequence
      - name: Dommy
        pol_account_idx: 1
        ffxi_character_idx: 0
        profile: EU_4k
        pol_keyboard_sequence: pol_keyboard_sequence
    windower:
      path: 'C:\Users\ksalva\windower\Windower.exe'
    ashita:
      path: 'C:\users\ksalva\ashita\Ashita-cli.exe'
    ```

    Description of fields in `ffxi_launcher_config.yaml`:

    (all keys/values are case sensitive)
    * `name`: the name of the character to launch
    * `pol_acount_idx`: zero-based index of PlayOnline member account (in PlayOnline)
    * `ffxi_character_idx`: zero-based index of Final Fantasy XI character (in game)
    * `profile`: windower/ashita profile to use for launching this character
    * `pol_keyboard_sequence`: relative path to saved keyboard sequence generated above and used to enter square enix password into PlayOnline (relative to the launcher config file)
    * `windower.path`: full path to Windower.exe
    * `ashita.path`: full path to Ashita exe

4. Double click `ffxi-launcher.py` to run the launcher in order to log in to one or more of the characters specified in your `ffxi_launcher_config.yaml` file.  (Note that the launcher requires admin privileges due to the win32 calls it makes to query the Windower processes, which runs as admin.  The launcher will automatically attempt to restart a new instance of the launcher with admin privleges if necessary.)  It is possible to pause/resume the launch process at any time by pressing `ctrl+alt+p` or to stop the launch process by pressing `ctrl+alt+s`.

# How to manage passwords

This launcher requires each account to have its PlayOnline password saved in PlayOnline and token authentication is not currently supported.  The Square Enix password is automated by the launcher via a sequence of clicks on the virtual keboard in PlayOnline.  This sequence of clicks is represented as a sequence of integers which is password protected and encrypted before finally being stored in a file.  One or more of these keyboard sequence files are associated with each account in the launcher config file.

Double click `pol-keyboard.py` to record and save your sequence of clicks to be used as the Square Enix password.  This will open up an image where you can click on each key on the virtual keyboard in sequence.  Note that there is no feedback during the clicking process.  Remember to click the "circle" key in the upper left when finished and the window should close.  Follow the remaining prompt to set a password if desired.  The command terminal will close when complete.  This process will need to be repeated for each Square Enix account that you want to launch, using a unique filename for each.

This method is secure enough to satisfy me, but security is not my specialty so I'm open to suggestions on how to improve this process.

# How to launch more than four accounts

This assumes FFXI and PlayOnline US are already installed.

1. Use regedit to export HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\PlayOnlineUS to a file.

2. Download and install EU PlayOnline to a different location than your US PlayOnline is installed (e.g., `C:\Program Files (x86)\PlayOnlineEU`).  Links to download EU and US PlayOnline provided below.
    * http://www.playonline.com/ff11eu/download/media/install_win.html
    * http://www.playonline.com/ff11us/download/media/install_win.html
    * http://dl.square-enix.co.jp/polus/setup.exe (outdated)
    * http://dl.square-enix.co.jp/poleu/setup.exe (outdated)

3. Run the US PlayOnline installer to repair the US install after installing EU PlayOnline.

4. Edit registry file exported above in notepad++.  Replace value of `HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\PlayOnlineEU\InstallFolder`:`1000` with path to where you installed EU PlayOnlinein step 2 (e.g., `C:\\Program Files (x86)\\PlayOnlineEU\\SquareEnix\\PlayOnlineViewer`).  Replace all occurrences of `PlayOnlineUS` with `PlanOnlineEU`.  Merge/import modified registry entries back into registry.

5. Create two profiles in Ashita/Windower, one for running the `US` version and another for running the `EU` version.

6. Ready to update, add accounts, and play ffxi!  Stop here unless you run into dll issues, then proceed to step 7.

7. How to use regsvr32 to register ffxi and pol dlls (this was not required the last time I went through this process so only do this if necessary).  This must be executed in/relative to the `C:\\Program Files (x86)\\PlayOnline\\SquareEnix` directory.

    ffxi:
    ```
    regsvr32 "FINAL FANTASY XI\FFXi.dll" /s
    regsvr32 "FINAL FANTASY XI\FFXiMain.dll" /s
    regsvr32 "FINAL FANTASY XI\FFXiVersions.dll" /s
    ```

    pol:
    ```
    regsvr32 "PlayOnlineViewer\viewer\ax\polmvfINT.dll" /s
    regsvr32 "PlayOnlineViewer\viewer\com\app.dll" /s
    regsvr32 "PlayOnlineViewer\viewer\com\polcore.dll" /s
    regsvr32 "PlayOnlineViewer\viewer\contents\polcontentsINT.dll" /s
    ```

# Problems and solutions

## PlayOnline accounts are corrupted

**Problem:**

Account names show up in PlayOnline containing strange/unexpected characters and symbols.  This can occur for a number of reasons but seems to occur more frequently when installing and running both US and EU PlayOnline on the same PC.

**Solution:**

Delete `C:\Program Files (x86)\PlayOnline\SquareEnix\PlayOnlineViewer\usr\all\login_w.bin` from the PlayOnlineViewer directory.  Remember to delete from both US and EU PlayOnlineViewer folders if you are using both US and EU versions.  Note that this will require reentering account details in PlayOnline but is easier than the alternative of completely reinstalling PlayOnline.

## The launcher stops clicking upon reaching the ffxi "accept" dialog

**Problem:**

The launcher gui opens, launches windower/POL, and the launcher does not perform any actions in POL, but there are no errors reported in the launcher.

**Solution:**

Try setting display resolution to 3840x2160 and setting the following in your `settings.xml` windower profile.  If this doesn't resolve the issue, then there is likely a bug and the code will need to be updated.
```
<width>3840</width>
<height>2160</height>
<mipmaplevel>1</mipmaplevel>
<gamma>1</gamma>
<uiscale>2</uiscale>
```
