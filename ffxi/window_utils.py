import re
import sys
from time import sleep, time
from easydict import EasyDict
from typing import Union, List
from pathlib import Path
import win32gui
import win32api
import win32process
import win32con
import win32com.client

import cv2
from PIL import Image
from mss import mss # ultrafast multiple screenshot module
import numpy as np

shell = win32com.client.Dispatch("WScript.Shell")
sct = mss()


window_activate_timeout = 2

# ref (https://stackoverflow.com/questions/44735798/pywin32-how-to-get-window-handle-from-process-handle-and-vice-versa)

class WindowList:
    def __init__(self):
        self.hwnd = []
        self.pid = []
        self.title = []
        self.pname = []
        win32gui.EnumWindows(self.enum_windows, None)

    def enum_windows(self, hwnd, _):
        if hwnd and win32gui.GetWindow(hwnd, win32con.GW_OWNER) == 0 and win32gui.IsWindowVisible(hwnd):
            _, pid = win32process.GetWindowThreadProcessId(hwnd)
            title = win32gui.GetWindowText(hwnd)
            try:
                handle = win32api.OpenProcess(win32con.PROCESS_QUERY_LIMITED_INFORMATION | win32con.PROCESS_VM_READ, False, pid)
                pname = Path(win32process.GetModuleFileNameEx(handle, 0)).name
                self.hwnd += [hwnd]
                self.pid += [pid]
                self.title += [title]
                self.pname += [pname]
            except:
                pass

def win_list(title_regex):
    '''
    finds all windows with title matching regex
    returns tuple of (title, hwnd, pid) for each matching window
    '''
    window_list = WindowList()
    try:
        idx = [i for i, title in enumerate(window_list.title) if re.match(title_regex, title)]
        return [EasyDict(title=window_list.title[i], handle=window_list.hwnd[i], pid=window_list.pid[i], pname=window_list.pname[i])  for i in idx]
    except:
        return []


def win_get_process_by_pid_and_title(pid, title):
    window_list = win_list('.*')
    for window in window_list:
        if window.pid == pid and window.title == title:
            return window
    return None


def win_get_process_by_pid(pid):
    window_list = win_list('.*')
    for window in window_list:
        if window.pid == pid:
            return window
    return None


def win_get_process_by_handle(handle):
    window_list = win_list('.*')
    for window in window_list:
        if window.handle == handle:
            return window
    return None


def win_get_processes_by_pname(pname):
    window_list = win_list('.*')
    return [window for window in window_list if window.pname == pname]


def get_process_creation_time(pid):
    # get the process creation time
    while True:
        try:
            phwnd = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION , 0, pid)
            process_times_dict = win32process.GetProcessTimes(phwnd)
            t = float(process_times_dict['CreationTime'].timestamp())
            return t
        except win32api.error as e:
            if e.winerror == 5: # access dennied (happens intermittently, not sure why so we repeat hoping it doesn't happen again)
                sleep(0.1)
                continue
            else: # any other winerror code treated as a failure
                break
        except: # any other exception treated as a failure
            break
    return sys.maxsize


def win_active_by_handle(hwnd):
    return win32gui.GetForegroundWindow() == hwnd


def win_activate_by_handle(hwnd):
    if win32gui.GetWindowPlacement(hwnd)[1] == win32con.SW_SHOWMINIMIZED:
        win32gui.ShowWindow(hwnd, win32con.SW_SHOWNORMAL)
    else:
        win32gui.SetForegroundWindow(hwnd)


def win_wait_active_by_handle(hwnd, timeout=window_activate_timeout):
    start = time()
    while True:
        if win_active_by_handle(hwnd):
            return True
        else:
            win_activate_by_handle(hwnd)
        if (time() - start) > timeout:
            return False


def win_wait_activate(hwnd, timeout=window_activate_timeout):
    try:
        if not win_active_by_handle(hwnd):
            shell.SendKeys('%') # work around to avoid exception/failing to activate when calling SetForegroundWindow/win_activate_by_handle below
            win_activate_by_handle(hwnd)
            win_wait_active_by_handle(hwnd, timeout=window_activate_timeout)
        return win_active_by_handle(hwnd)
    except:
        return False


def screen_capture_window(hwnd):
    im = None
    try:
        x1, y1, x2, y2 = win32gui.GetWindowRect(hwnd)
        bbox = {"top": y1, "left": x1, "width": x2 - x1, "height": y2 - y1}
        sct_im = sct.grab(bbox)
        im = Image.frombytes("RGB", sct_im.size, sct_im.bgra, "raw", "BGRX")
    except:
        pass

    return im


def scale_search_region(search_region, xscale, yscale, uiscale, xmax, ymax):
    w = search_region[2] - search_region[0]
    h = search_region[3] - search_region[1]
    w2 = w / 2
    h2 = h / 2
    cx = search_region[0] + w2
    cy = search_region[1] + h2
    cx = cx * xscale
    cy = cy * yscale
    w2 = w2 * xscale * max(uiscale, 1.0)
    h2 = h2 * yscale * max(uiscale, 1.0)

    tlx = int(cx - w2)
    tly = int(cy - h2)
    brx = int(cx + w2)
    bry = int(cy + h2)
    if tlx < 0:
        brx = brx - tlx
        tlx = 0
    if tly < 0:
        bry = bry - tly
        tly = 0
    if brx > (xmax - 1):
        tlx = tlx - (brx - xmax)
        brx = xmax - 1
    if bry > (ymax - 1):
        tly = tly - (bry - ymax)
        bry = ymax - 1

    return tlx, tly, brx, bry

def locate_all_in_window(hwnd: int,
                         template: Union[str, List[str]],
                         scale: float = 1.0,
                         uiscale: float = 1.0,
                         threshold: float = 0.9,
                         search_region: list = None,
                         display: bool = False):
    # make sure template is a list to support matching multiple templates
    template_orig = template
    if isinstance(template, list):
        assert isinstance(scale, list)
        assert isinstance(threshold, list)
        assert isinstance(search_region, list)
        templates = template
        scales = scale
        uiscales = uiscale
        thresholds = threshold
        search_regions = search_region
    else:
        templates = [template]
        scales = [scale]
        uiscales = [uiscale]
        thresholds = [threshold]
        search_regions = [search_region]

    # capture screenshot of window
    imref = screen_capture_window(hwnd)
    if imref is None:
        return []
    imref_gray = imref.convert('L')
    ref = np.asarray(imref)
    ref_gray = np.asarray(imref_gray)

    result_list = []
    for template, scale, uiscale, threshold, search_region in zip(templates, scales, uiscales, thresholds, search_regions):
        # convert template to Image if needed
        if not isinstance(template, Image.Image):
            template = Image.fromarray(template)

        # convert template to np.array
        template = np.asarray(template)

        # rescale template to match windower
        xscale = scale
        yscale = scale
        if type(scale) in [list, tuple]:
            xscale = scale[0]
            yscale = scale[1]
        if uiscale != 1:
            w = int(template.shape[1] * uiscale)
            h = int(template.shape[0] * uiscale)
            template = cv2.resize(template, (w, h))

        # choose to match color or gray
        if template.ndim == 3 and template.shape[2] > 1:
            ref_to_match = ref
        else:
            ref_to_match = ref_gray

        # crop image to reduce search region
        if search_region:
            tlx, tly, brx, bry = scale_search_region(search_region, xscale, yscale, uiscale, ref_to_match.shape[1], ref_to_match.shape[0])
            ref_cropped = ref_to_match[tly:bry, tlx:brx]
        else:
            tlx = 0
            tly = 0
            ref_cropped = ref_to_match

        # compute cross correlation
        try:
            corr_response = cv2.matchTemplate(ref_cropped, template, cv2.TM_CCOEFF_NORMED)
        except:
            continue

        w, h = template.shape[:2][::-1]
        loc = np.where( corr_response >= threshold)

        # find centroids of connected components
        mask = np.zeros(shape=corr_response.shape, dtype=np.uint8)
        mask[loc[0], loc[1]] = 255
        output = cv2.connectedComponentsWithStats(mask, connectivity=8)
        centroids = output[3]
        centroids = centroids[1:]
        centroids = [(int(c[0]), int(c[1])) for c in centroids]

        if display:
            ref_overlay = ref.copy()
        result = []
        for pt in centroids:
            x = pt[0]
            y = pt[1]
            score = corr_response[y, x]
            result += [EasyDict(x=int(x + tlx + w / 2), y=int(y + tly + h / 2), score=score)]
            if display:
                ref_overlay = cv2.rectangle(ref_overlay, (x, y), (x + w, y + h), 255, 2)
        if display:
            corr_response = cv2.normalize(corr_response,  None, 0, 255, cv2.NORM_MINMAX)
            cv2.imshow('ref_overlay', ref_overlay)
            cv2.imshow('ref', ref)
            cv2.imshow('ref_cropped', ref_cropped)
            cv2.imshow('template', template)
            cv2.imshow('corr_response', corr_response.astype(np.uint8))
            cv2.waitKey(0)
        # cv2.imwrite('ref.png', ref)
        # cv2.imwrite('ref_cropped.png', ref_cropped)
        # cv2.imwrite('template.png', template)

        result = sorted(result, key=lambda item: item.score, reverse=True)
        result_list.append(result)

    if isinstance(template_orig, list):
        return result_list
    else:
        return result_list[0]
