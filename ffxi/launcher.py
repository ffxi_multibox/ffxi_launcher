import os
import sys
import ctypes
import argparse
import yaml
from easydict import EasyDict
from functools import partial
from pathlib import Path
from time import sleep
import colorsys
import logging
import traceback
import keyboard

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore

from ffxi.pol import Window, VirtualKeyboard


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
try:
    fh = logging.FileHandler('launcher.log', mode='w')
    logger.addHandler(fh)
except:
    pass
ch = logging.StreamHandler()
logger.addHandler(ch)


def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


class FFXIChar(QtCore.QObject):

    logged = QtCore.pyqtSignal(str, str)

    def __init__(self, name=None, pol_account_idx=None, ffxi_character_idx=None, exe_path=None, virtual_keyboard=None, profile=None, ashita=False, dpi_scale=1.0, **kwargs):
        super().__init__()
        if name is None or exe_path is None:
            raise RuntimeError('name or exe_path is None')
        self.name = name
        self.pol_account_idx = pol_account_idx
        self.ffxi_character_idx = ffxi_character_idx
        self.exe_path = exe_path
        self.profile = profile
        self.window = Window(name, pol_account_idx, ffxi_character_idx, exe_path, virtual_keyboard, profile, ashita, dpi_scale)

    def log(self, message, name=None):
        self.logged.emit(message, name)

    def reset(self):
        self.window.reset()

    def next_step(self):
        return self.window.next_step()

    def done(self):
        return self.window.done()

class FFXITeam(QtCore.QObject):

    logged = QtCore.pyqtSignal(str, str)
    finished = QtCore.pyqtSignal()

    def __init__(self, config_path, on_logged, dpi_scale=1.0, debug=False):
        super().__init__()
        # connect log signals to slots
        self.logged.connect(on_logged)
        self._debug = debug
        self._pause = False
        self._stop = False
        self.config = self.load_config(config_path)
        ashita = True if Path(self.config.get('ashita', {}).get('path', '')).is_file() else False
        exe_path = self.config.ashita.path if ashita else self.config.windower.path
        exe_path = Path(exe_path).resolve()
        self.chars = []
        if self.config is not None:
            virtual_keyboards = self.load_virtual_keyboards(self.config, Path(config_path).parent)
            # instantiate each ffxi character in team
            for char in self.config.team:
                self.chars += [FFXIChar(**char, exe_path=exe_path, virtual_keyboard=virtual_keyboards[char.name], ashita=ashita, dpi_scale=dpi_scale)]
                # connect log signals to slots
                self.chars[-1].logged.connect(on_logged)
                self.chars[-1].window.logged.connect(on_logged)

    def load_config(self, config_path):
        try:
            assert Path(config_path).is_file(), f'{config_path} file does not exist'
        except AssertionError as e:
            self.log(e)
            sys.exit(1)
        try:
            with open(config_path, 'r') as stream:
                config = EasyDict(yaml.safe_load(stream))
                return config
        except Exception as e:
            self.log(e)
            sys.exit(1)

    def load_virtual_keyboards(self, config, parent_path):
        # determine the unique set of virtual keyboards and load them from file, prompting for password if required
        character_keyboard_map = {player.name: player.get('pol_keyboard_sequence', None) for player in config.team}
        keyboard_paths = set([player.get('pol_keyboard_sequence', None) for player in config.team])
        for keyboard_path in keyboard_paths:
            if keyboard_path is None:
                continue
            try:
                virtual_keyboard = VirtualKeyboard(sequence_path=str(Path(parent_path) / keyboard_path))
                virtual_keyboard.load()
            except Exception as e:
                self.log(e)
                sys.exit(1)
            for player_name, keyboard_path2 in character_keyboard_map.items():
                if keyboard_path == keyboard_path2:
                    character_keyboard_map[player_name] = virtual_keyboard
        return character_keyboard_map

    def log(self, message, name=None):
        self.logged.emit(str(message), name)

    def launch(self, char_indices=[]):
        if self._debug:
            try:
                import ptvsd
                ptvsd.debug_this_thread()
            except:
                pass
        try:
            keyboard.add_hotkey('ctrl+alt+p', self.pause, suppress=False)
            keyboard.add_hotkey('ctrl+alt+s', self.stop, suppress=False)
            keyboard.add_hotkey('esc', self.stop, suppress=False)
            chars = []
            for idx in char_indices:
                char = self.chars[idx]
                char.reset() # reset to first step
                chars.append(char)

            # luanch pol and perform login process on all characters
            while not all([char.done() for char in chars]):
                for char in chars:
                    char.next_step()
                    if self._pause:
                        if not self._stop:
                            self.log('Paused')
                        while self._pause and not self._stop:
                            sleep(0.1)
                        if not self._stop:
                            self.log('Resumed')
                    if self._stop:
                        self.log('Stopped')
                        break
                if self._stop:
                    break

            keyboard.remove_all_hotkeys()
            self._pause = False
            self._stop = False

        except Exception as e:
            self.log(e)
            self.log(traceback.format_exc())

        finally:
            # emit signal to log and let others know launching is finished
            self.log('Done')
            # self.moveToThread(QtCore.QCoreApplication.instance().thread()) # if we don't want to reuse the thread, need to move the worker back to main thread
            self.finished.emit()

    def size(self):
        return len(self.chars)

    def pause(self):
        self._pause = not self._pause
        if self._pause:
            self.log('Pausing')
        else:
            self.log('Resuming')

    def stop(self):
        self.log('Stopping')
        self._stop = True


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, config_path, debug=False):
        super().__init__()
        self.move(0, 0)
        screen = self.screen()
        dpi_scale = screen.logicalDotsPerInch() / 96.0
        logger.info(f'dpi scale: {dpi_scale}')
        self.ffxi_team = FFXITeam(config_path, self.on_logged, dpi_scale, debug)
        self.character_names = [char.name for char in self.ffxi_team.chars]
        # self.config_path = config_path
        # self.config = FFXITeam.load_config(self.config_path)
        # self.character_names = [char.name for char in self.config.team]
        self.colormap = MainWindow.get_colors(self.character_names)

        self.widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.widget)

        self.layout = QtWidgets.QGridLayout()

        self.character_table_label = QtWidgets.QLabel('Select characters to launch:')

        self.character_table = QtWidgets.QTableWidget(len(self.character_names), 1)
        self.character_table.verticalHeader().setVisible(False)
        self.character_table.horizontalHeader().setVisible(False)
        self.character_table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.character_table.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.character_table.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.character_table.setItemDelegate(MainWindow.ColorDelegate(character_names=self.character_names))
        self.character_table.setFocusPolicy(QtCore.Qt.NoFocus)
        colors = MainWindow.get_colors(self.character_names)
        for i, name in enumerate(self.character_names):
            item = QtWidgets.QTableWidgetItem(name)
            item.setForeground(QtGui.QColor(colors[name][0], colors[name][1], colors[name][2]))
            item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
            self.character_table.setItem(i, 0, item)

        self.status_text = QtWidgets.QTextEdit()
        self.status_text.setReadOnly(True)

        self.launch_button = QtWidgets.QPushButton('Launch')
        self.launch_button.clicked.connect(self.launch_team)

        self.layout.addWidget(self.character_table_label, 0, 0)
        self.layout.addWidget(self.character_table, 1, 0)
        self.layout.addWidget(self.status_text, 2, 0)
        self.layout.addWidget(self.launch_button, 3, 0)

        self.widget.setLayout(self.layout)

        self.setWindowTitle('FFXI Multibox Launcher')
        self.adjustSize()

        self.move(screen.size().width() - self.size().width(), screen.size().height() - self.size().height() - int(dpi_scale * (48 + 32)))  # task bar is 48 px and window tile is 32 px

        self.mutex  = QtCore.QMutex()
        self.thread = None

    def launch_team(self):
        if self.mutex.tryLock():
            if self.thread is None:
                # create thread
                self.thread = QtCore.QThread()

                # connect finished signal to slot
                self.ffxi_team.finished.connect(self.launch_finished)

                # proper worker/thread cleanup below
                # ffxi_team.finished.connect(self.thread.quit) # 1
                # ffxi_team.finished.connect(ffxi_team.deleteLater) # 2
                # self.thread.finished.connect(self.thread.deleteLater) # 3
                self.ffxi_team.moveToThread(self.thread)
            else:
                self.thread.started.disconnect()

            # run ffxi_team.launch in thread on selected characters
            self.thread.started.connect(partial(self.ffxi_team.launch, [item.row() for item in self.character_table.selectedItems()]))
            self.thread.start()

    @QtCore.pyqtSlot()
    def launch_finished(self):
        self.thread.quit()
        self.thread.wait() # wait is necessary here or application will terminate
        self.mutex.unlock()

    @QtCore.pyqtSlot(str, str)
    def on_logged(self, message, name):
        if name:
            if hasattr(self, 'status_text'):
                r, g, b = self.colormap[name]
                self.status_text.append(f'<font color="#{r:02x}{g:02x}{b:02x}">{name}</font>: {message}')
            logger.info(f'{name}: {message}')
        else:
            if hasattr(self, 'status_text'):
                self.status_text.append(f'{message}')
            logger.info(f'{message}')

    @staticmethod
    def get_colors(charater_names):
        n = len(charater_names)
        # min/max hue to filter out
        hmin = 45 # beginning of yellow
        hmax = 120 # middle of green

        # compute hsv colors
        hsv_tuples = [((x*(360.0 - hmax + hmin)/360.0/n + float(hmax)/360.0) % 1.0, 1.0, 0.9) for x in range(n)]

        # convert hsv to rgb
        rgb_tuples = [colorsys.hsv_to_rgb(*x) for x in hsv_tuples]
        rgb_tuples = [(int(x[0]*255), int(x[1]*255), int(x[2]*255)) for x in rgb_tuples] # scale from 0-1 to 0-255
        rgb_map = EasyDict()
        for i, name in enumerate(charater_names):
            rgb_map[name] = rgb_tuples[i]

        return rgb_map
        # for i in range(20):
        #     for r,g,b in rgb_tuples:
        #         status_text.append(f'<font color="#{r:02x}{g:02x}{b:02x}">hello</font> world')
        #         # status_text.append('<font color="Lime">hello world<font color=\"Black\"> goodbye')

    class ColorDelegate(QtWidgets.QStyledItemDelegate):

        def __init__(self, *args, character_names=None, **kwargs):
            super().__init__(*args, **kwargs)
            self.colormap = MainWindow.get_colors(character_names)

        def paint(self, painter, option, index):
            color = self.colormap[index.data()]
            option.palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(color[0], color[1], color[2]))
            option.palette.setColor(QtGui.QPalette.HighlightedText, QtGui.QColor(255, 255, 255))
            QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)


def parse_args():
    # defaults
    config = 'ffxi_launcher_config.yaml'

    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to multibox team config file", type=str, default=config)
    parser.add_argument("--debug", help="enable ptvsd for debugging", action="store_true")

    return parser.parse_args()


# for debugging:
# python -m ptvsd --host localhost --port 5678 --wait -m ffxi.launcher --debug
def main():
    if not is_admin():
        logger.info('not admin... restarting as admin')

        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, 'runas', str(Path(sys.exec_prefix) / 'pythonw'), ' '.join(sys.argv), os.getcwd(), 1)
        sys.exit(0)
    else:
        logger.info('currently running as admin')

        args = parse_args()

        app = QtWidgets.QApplication([])

        window = MainWindow(args.config, args.debug)

        window.show()

        app.exec_()

if __name__== '__main__':
    main()
