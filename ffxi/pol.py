from __future__ import annotations
from abc import abstractmethod
from enum import Enum
import re
import os
import sys
import subprocess
import argparse
from typing import Callable, List
from time import time, sleep
import warnings
import win32gui
import win32con
import win32api
import winreg
from functools import partial
from easydict import EasyDict
from pathlib import Path
import numpy as np
import tkinter as tk
from PIL import Image, ImageTk
import cv2
import autoit
import xml.etree.ElementTree as ET
import configparser

# crypto tools
import getpass
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet, InvalidToken

from PyQt5 import QtCore

from ffxi.window_utils import win_list, win_get_process_by_pid_and_title, win_get_process_by_pid, locate_all_in_window, win_wait_activate, get_process_creation_time


default_mouse_speed = 1
default_locate_threshold = 0.8
ffxi_launch_timeout = 30

data_dir = Path(__file__).parent

class Clickable:

    class SortOrder(Enum):
        LEFT_TO_RIGHT_TOP_TO_BOTTOM = 1
        TOP_TO_BOTTOM_LEFT_TO_RIGHT = 2

    def __init__(self,
                 message: str,
                 path: str,
                 result: bool,
                 pre_click_callback: Callable,
                 post_click_callback: Callable,
                 scale: float = 1,
                 uiscale: float = 1.0,
                 threshold: float = default_locate_threshold,
                 click_callback: Callable = None,
                 search_region: list = None,
                 count: int = 1,
                 index: int = 0,
                 click: bool = True,
                 color: bool = False,
                 sort: SortOrder = SortOrder.LEFT_TO_RIGHT_TOP_TO_BOTTOM
                 ):
        """
        data structure for clickable item

        Arguments:
            message {str} -- message to log on success
            path {str} -- path to template image to find and click on
            result {bool} -- result to set on success, inverse will be set on failure
            pre_click_callback {Callable} -- callback function triggered when click region is found, before click action is performed
            post_click_callback {Callable} -- callback function triggered when click region is found, after click action is performed
        """
        self.message = message
        self.path = path
        self.scale = scale
        self.uiscale = uiscale
        self.threshold = threshold
        self.search_region = search_region
        self.count = count
        self.result = result
        self.pre_click_callback = pre_click_callback
        self.click_callback = click_callback
        self.post_click_callback = post_click_callback
        self.index = index
        self.click = click
        self.color = color
        self.sort = sort

class Window(QtCore.QObject):

    logged = QtCore.pyqtSignal(str, str)

    pol_window_title = 'PlayOnline Viewer.*'
    final_fantasy_window_title = 'Final Fantasy XI'
    image_path = data_dir / 'images'
    claimed_windows = []
    slot_count = 0
    waiting_for_launch = False
    waiting_for_ffxi = False
    # the following flag allows each character to use its own unique windower profile if desired for different ffxi settings
    enable_multiple_windower_profiles = False

    def __init__(self, char_name, pol_account_idx, ffxi_character_idx, exe_path, virtual_keyboard, profile=None, ashita=False, dpi_scale=1.0):
        super().__init__()
        self.window = None
        self.char_name = char_name
        self.pol_account_idx = pol_account_idx
        self.ffxi_character_idx = ffxi_character_idx
        self.ashita = ashita
        self.exe_path = exe_path
        self.profile = profile
        self.dpi_scale = dpi_scale # windows dpi scaling is needed to correctly determine scale of correlation template for locating ui elements to click on
        if self.pol_account_idx is None:
            self.steps = [
                partial(self.launch, private=True),
                # ffxi login
                self.__ffxi_login,
            ]
        else:
            self.steps = [
                self.launch,
                # pol login
                self.__pol_login,
                # ffxi login
                self.__ffxi_login,
            ]
        self.step_idx = 0
        self.virtual_keyboard = virtual_keyboard
        self.slot = Window.__get_slot()
        self._ffxi_character_slot_click_time = 0

    def log(self, message):
        self.logged.emit(str(message), self.char_name)

    def launch(self, private=False):
        # windower does not handle launching instances concurrently very well, but ashita seems to handle this better
        if Window.waiting_for_launch:
            return False
        Window.waiting_for_launch = True
        self.__update_window()
        if self.window is None:
            # launch windower
            self.log('Launching!')
            if self.ashita:
                self.settings = AshitaSettings(self.exe_path.parent, self.profile)
                subprocess.Popen([os.path.normpath(self.exe_path), f'{self.profile}.ini'], shell=True, cwd=self.exe_path.parent)
                sleep(1.0)
            else:
                self.settings = WindowerSettings(self.exe_path.parent, self.profile)
                subprocess.Popen([os.path.normpath(self.exe_path), f'-p={self.profile}'], shell=True, cwd=self.exe_path.parent)
            self.step_idx = 0

            if private:
                result = self.__pol_wait_for_ffxi()
            else:
                result = self.__wait_for_pol()
            return result
        else:
            self.log('Skipping... process already exists')
            self.step_idx = len(self.steps)
            Window.waiting_for_launch = False
            return False

    def reset(self):
        self.step_idx = 0
        Window.waiting_for_launch = False

    def __wait_for_pol(self):
        self.__update_window()
        if self.window is None:
            # if self.ashita:
            #     window = self.__get_next_available()
            #     if not window:
            #         return False
            #     if not win32gui.IsWindowVisible(window.handle):
            #         return False
            #     self.window = window
            #     self.__set_window_position()
            #     Window.waiting_for_launch = False
            #     return True
            # else:
            timeout = 30
            start = time()
            while (time() - start) < timeout:
                self.window = self.__get_next_available()
                if self.window:
                    while not win32gui.IsWindowVisible(self.window.handle):
                        sleep(0.1)
                    self.__set_window_position()
                    Window.waiting_for_launch = False
                    return True
                sleep(0.1)
            Window.waiting_for_launch = False
            raise Exception('Could not find unclaimed instance of PlayOnline with Windower enabled')
        else:
            return True

    def __get_ffxi_scale(self):
        self.__update_window()
        self.__get_ffxi_settings()
        try:
            left, top, right, bottom = win32gui.GetWindowRect(self.window.handle)
            w = float(right - left)
            h = float(bottom - top)
            xscale = w / 3840
            yscale = h / 2160
            return xscale, yscale
        except:
            return None

    def __get_ffxi_uiscale(self):
        uiscale = self.uiscale
        if self.screen_res_w and self.screen_res_h:
            try:
                left, _, right, _ = win32gui.GetWindowRect(self.window.handle)
                w = float(right - left)
                uiscale = uiscale * w / self.screen_res_w
            except:
                pass
        return uiscale

    def __set_window_position(self):
        # Windows may scale applications on high dpi monitors, which will screw up pol and ffxi scale.
        # We can account for the scale difference in pol, but this may result in content displaying off screen in ffxi.
        # See below for the issue.  The current best solution is to right click on pol and disable scaling in high dpi compatibility settings.
        # http://forums.windower.net/index.php@%252Ftopic%252F625-windower-4k-issue%252F%25C3%2582%25C2%25A0.html

        # wait for window to fully initialize
        while not self.__get_window_offset():
            sleep(0.1)
        sleep(0.5)

        # set window pos and size
        w = int(656 * self.dpi_scale)
        h = int(519 * self.dpi_scale)
        x = (self.slot - 1) % 3 * w
        y = int((self.slot - 1) / 3) * h
        win32gui.SetWindowPos(self.window.handle, win32con.HWND_TOP , x, y, w, h, 0)
        sleep(0.5)

    def __update_window(self, timeout=0.0):
        # first, check if window with pid still exists
        if self.window is not None:
            window = None
            start = time()
            while window is None:
                window = win_get_process_by_pid(self.window.pid)
                if window is not None or (time() - start) > timeout:
                    break
                else:
                    sleep(0.1)
            if window:
                # update window info
                self.window = window
            elif not Window.waiting_for_ffxi:
                # if window no longer exists, remove claimed window and reset this window to allow finding a new window
                Window.claimed_windows = [win for win in Window.claimed_windows if win.pid != self.window.pid]
                self.window = None
        if self.window is None:
            windows = win_list(f'^{self.char_name}$')
            if not windows:
                windows = win_list(f'^{Window.final_fantasy_window_title}$')
            if not windows:
                windows = win_list(f'^{Window.final_fantasy_window_title.upper()}$')
            if len(windows) > 0:
                # self.log('claiming window')
                self.window = windows[0]
                # add to claimed windows list if not already in list
                claimed_pids = [window.pid for window in Window.claimed_windows]
                if self.window.pid not in claimed_pids:
                    Window.claimed_windows += [self.window]
            return

    def activate(self, timeout=0):
        if self.window is not None:
            sleep(0.2)
            win_wait_activate(self.window.handle, timeout)
            sleep(0.2)

    def locate_and_move(self, template, scale=1.0, uiscale=1.0, timeout=0, **kwargs):
        assert not isinstance(template, list)
        loc = self.locate(template, scale=scale, uiscale=uiscale, timeout=timeout, **kwargs)
        if len(loc) > 0:
            self.mouse_move(loc[0], **kwargs)
            return True
        else:
            return False

    def locate_and_click(self, template, scale=1.0, uiscale=1.0, timeout=0, **kwargs):
        assert not isinstance(template, list)
        loc = self.locate(template, scale=scale, uiscale=uiscale, timeout=timeout, **kwargs)
        if loc:
            sleep(0.1)
            self.mouse_click(loc[0], **kwargs)
            return True
        else:
            return False

    def locate(self, template, scale=1.0, uiscale=1.0, timeout=0, color=False, search_region=None, **kwargs):
        threshold = kwargs.get('threshold', default_locate_threshold)
        template_orig = template
        if isinstance(template, str):
            assert isinstance(scale, (int, float, Callable))
            assert isinstance(threshold, (int, float))
            assert isinstance(color, bool)
            if search_region:
                assert isinstance(search_region, (list, tuple))
            if color:
                template = [Image.open(template).convert('L')]
            else:
                template = [Image.open(template)]
            scale_value = [scale() if callable(scale) else scale]
            uiscale_value = [uiscale() if callable(uiscale) else uiscale]
            threshold = [threshold]
            search_region = [search_region]
        elif isinstance(template, list):
            assert isinstance(scale, list)
            assert isinstance(threshold, list)
            assert isinstance(color, list)
            assert isinstance(search_region, list)
            template = [Image.open(t) if c else Image.open(t).convert('L') for t,c in zip(template, color)]
            scale_value = [s() if callable(s) else s for s in scale]
            uiscale_value = [s() if callable(s) else s for s in uiscale]

        display = kwargs.get('display', False)
        loc = []
        start = time()
        while True:
            self.__update_window()
            if isinstance(scale, (int, float, Callable)):
                scale_value = [scale() if callable(scale) else scale]
                uiscale_value = [uiscale() if callable(uiscale) else uiscale]
            elif isinstance(scale, list):
                scale_value = [s() if callable(s) else s for s in scale]
                uiscale_value = [s() if callable(s) else s for s in uiscale]
            try:
                loc = locate_all_in_window(self.window.handle, template, scale=scale_value, uiscale=uiscale_value, threshold=threshold, search_region=search_region, display=display)
            except:
                return []
            if any([l for l in loc]):
                if isinstance(template_orig, list):
                    return loc
                else:
                    return loc[0]
            if (time() - start) > timeout:
                return []
            sleep(0.1)

    def mouse_move(self, loc, speed=default_mouse_speed, **kwargs):
        while not self.__get_window_offset():
            sleep(0.1)
        xoffset, yoffset = self.__get_window_offset()
        autoit.mouse_move(loc.x + xoffset, loc.y + yoffset, speed=speed)

    def mouse_click(self, loc=None, speed=default_mouse_speed, **kwargs):
        if loc:
            while not self.__get_window_offset():
                sleep(0.1)
            xoffset, yoffset = self.__get_window_offset()
            autoit.mouse_click(x=loc.x + xoffset, y=loc.y + yoffset, speed=speed)
        else:
            autoit.mouse_click()

    def send_key(self, key):
        self.activate()
        autoit.send(key)

    def __get_window_offset(self):
        self.__update_window()
        try:
            x, y, _, _ = win32gui.GetWindowRect(self.window.handle)
            if x >= 0 and y >= 0:
                return x, y
            else:
                return None
        except:
            return None

    def next_step(self):
        result = False
        if self.step_idx < len(self.steps):
            try:
                result = self.steps[self.step_idx]()
            except Exception as e:
                self.log(e)
            if result:
                self.step_idx += 1
                if self.step_idx == len(self.steps):
                    self.log('Done')
        return result

    def set_step(self, idx):
        assert idx < len(self.steps)
        self.step_idx = idx

    def done(self):
        window_existed_before = self.window is not None
        self.__update_window()
        window_exists_now = self.window is not None
        if window_existed_before and not window_exists_now:
            self.step_idx = 0
        return False if self.step_idx < len(self.steps) else True

    def __pol_login(self):
        self.activate()

        # login / connect
        if self.__pol_connect():
            return False

        clickables = [
            # error handling
            Clickable(
                message='Connection error, retrying',
                path=Window.get_image_path('network_communication_error_retry.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[328, 408, 500, 444]
                ),
            Clickable(
                message='Connection error, reconnecting',
                path=Window.get_image_path('network_connection_error_exit.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=partial(self.set_step, 1),
                search_region=[472, 400, 600, 456]
                ),
            # mail updated notification
            Clickable(
                message='Click mail info updated "ok"',
                path=Window.get_image_path('pol_mail_info_updated_ok.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None
                ),
            # crystal shortcut button
            Clickable(
                message='Click "crystal shorcut"',
                path=Window.get_image_path('5_pol_crystal_shortcut.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[8, 160, 56, 216]
                ),
            # ffxi button
            Clickable(
                message='Click "Final Fantasy XI"',
                path=Window.get_image_path('5_pol_ffxi.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[40, 160, 232, 208]
                ),
            # upcoming update notification
            Clickable(
                message='Click notification "close"',
                path=Window.get_image_path('6-1_pol_close_notification.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                # TODO search region
                # search_region=[]
                ),
            # first play button
            Clickable(
                message='Click first "play"',
                path=Window.get_image_path('6_pol_play.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[56, 96, 256, 128]
                ),
            # yes go online (after network error when starting ffxi)
            Clickable(
                message='Yes go online"',
                path=Window.get_image_path('yes_go_online.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=partial(self.__false_wrapper, self.__pol_do_keyboard_magic_and_click_connect),
                # TODO search region
                # search_region=[]
                ),
            ]

        if not Window.waiting_for_ffxi:
            clickables += [
                # second play button
                Clickable(
                    message='Click second "play"',
                    path=Window.get_image_path('7_pol_play.png'),
                    scale=self.dpi_scale,
                    uiscale=self.dpi_scale,
                    result=True,
                    pre_click_callback=self.__update_ffxi_settings,
                    post_click_callback=self.__pol_wait_for_ffxi,
                    search_region=[136, 400, 288, 432]
                    ),
                ]

        result = self.__click_list(clickables)
        return result

    def __false_wrapper(self, func):
        func()
        return False

    def __ffxi_login(self):
        self.activate()

        def stop_waiting_for_ffxi():
            Window.waiting_for_ffxi = False

        clickables = [
            # accept button
            Clickable(
                message='Click "accept"',
                path=Window.get_image_path('8_ffxi_login.png'),
                scale=self.__get_ffxi_scale,
                uiscale=self.__get_ffxi_uiscale,
                threshold=0.8,
                result=False,
                pre_click_callback=None,
                post_click_callback=stop_waiting_for_ffxi,
                search_region=[1700, 1000, 2300, 1300]
                ),
            # network error
            # Clickable(
            #     message='Network error, exiting and starting over',
            #     path=Window.get_image_path('8_ffxi_login_error.png'),
            #     scale=self.__get_ffxi_scale,
            #     uiscale=self.__get_ffxi_uiscale,
            #     result=False,
            #     pre_click_callback=None,
            #     post_click_callback=partial(self.set_step, 1),
            #     search_region=[700, 300, 1200, 800]
            #     ),
            # select character
            Clickable(
                message='Click "select character"',
                path=Window.get_image_path('9_ffxi_select_character.png'),
                scale=self.__get_ffxi_scale,
                uiscale=self.__get_ffxi_scale()[0],  # not sure why scaling is different on this screen only (uiscale has no impact)
                threshold=0.8,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[1000, 1100, 2200, 1400]
                ),
            # yes
            Clickable(
                message=f'Click "yes"',
                path=Window.get_image_path('11_ffxi_yes.png'),
                scale=self.__get_ffxi_scale,
                uiscale=self.__get_ffxi_uiscale,
                threshold=0.9,
                result=True,
                pre_click_callback=None,
                post_click_callback=partial(sleep, 0.5),
                search_region=[1700, 1000, 2200, 1200]
                )
            ]

        if time() - self._ffxi_character_slot_click_time > 1:
            clickables += [
                # character slot
                Clickable(
                    message=f'Click "{self.char_name}"',
                    # path=Window.get_image_path('10_ffxi_select_character_to_play.png'),
                    path=Window.get_image_path('10_ffxi_select_character.png'),
                    scale=self.__get_ffxi_scale,
                    uiscale=self.__get_ffxi_uiscale,
                    threshold=0.8,
                    result=False,
                    pre_click_callback=None,
                    click_callback=self.__click_ffxi_character_slot,
                    post_click_callback=self.__set_ffxi_character_slot_clicked,
                    # search_region=[1600, 2000, 2400, 2200]
                    search_region=[3000, 1600, 3839, 2159]
                    ),
            ]

        return self.__click_list(clickables)

    def __click_ffxi_character_slot(self, loc):
        # calculate the location of the ffxi character slot and click
        uiscale = self.__get_ffxi_uiscale()
        xoffset = 100 * uiscale
        yoffset = 142 * uiscale / 8
        xleft = int(loc.x - xoffset)
        xright = int(loc.x + xoffset)
        ytop = loc.y - 3.5 * yoffset
        # ybottom = loc.y + 3.5 * yoffset
        x = xleft if self.ffxi_character_idx < 8 else xright
        y = int(round(ytop + yoffset * (self.ffxi_character_idx % 8)))
        click_loc = EasyDict(x=x, y=y)
        self.mouse_click(click_loc, speed=1)

    def __set_ffxi_character_slot_clicked(self):
        self._ffxi_character_slot_click_time = time()

    def __pol_do_keyboard_magic_and_click_connect(self):
        result = self.__pol_do_keyboard_magic()
        if result:
            result = self.__pol_click_connect()
        return result

    def __pol_connect(self):
        result = self.__pol_select_account()
        if result:
            result = self.__pol_click_login()
        if result:
            result = self.__pol_do_keyboard_magic_and_click_connect()
        return result

    def __pol_select_account(self):
        clickables = [
            Clickable(
                message='Select account',
                path=Window.get_image_path('1_pol_account.png'),
                scale=self.dpi_scale,
                uiscale=self.dpi_scale,
                result=True,
                count=4,
                pre_click_callback=None,
                post_click_callback=None,
                index=self.pol_account_idx,
                sort=Clickable.SortOrder.TOP_TO_BOTTOM_LEFT_TO_RIGHT,
                search_region=[216, 128, 440, 376]
            )
        ]

        return self.__click_list(clickables)

    def __pol_click_login(self):
        path = Window.get_image_path('2_pol_login.png')
        if not self.locate_and_click(path, scale=self.dpi_scale, uiscale=self.dpi_scale, timeout=2):
            self.__locate_failed()
            return False
        self.log('Click "login"')
        return True

    def __pol_do_keyboard_magic(self):
        path = Window.get_image_path('3_pol_password_box.png')
        if not self.locate_and_click(path, scale=self.dpi_scale, uiscale=self.dpi_scale, timeout=2):
            self.__locate_failed()
            return False

        path = Window.get_image_path('pol_keyboard.png')
        if not self.locate(path, scale=self.dpi_scale, uiscale=self.dpi_scale, timeout=2):
            self.__locate_failed()
            return False
        sleep(0.2)

        self.log('Virtual keyboard magic')
        locs = self.virtual_keyboard.get_locations()
        for loc in locs:
            loc_scaled = EasyDict(x=int(round(loc.x*self.dpi_scale)), y=int(round(loc.y*self.dpi_scale)))
            self.mouse_click(loc_scaled, speed=default_mouse_speed)
            sleep(0.1)
        sleep(0.1)
        self.log('Virtual keyboard magic complete')
        return True

    def __pol_click_connect(self):
        path = Window.get_image_path('4_pol_connect.png')
        if not self.locate_and_click(path, scale=self.dpi_scale, uiscale=self.dpi_scale, timeout=2):
            self.__locate_failed()
            return False
        self.log('Click "connect"')
        return True

    def __click_list(self, clickables: List[Clickable]):
        paths = [clickable.path for clickable in clickables]
        scales = [clickable.scale for clickable in clickables]
        uiscales = [clickable.uiscale for clickable in clickables]
        thresholds = [clickable.threshold for clickable in clickables]
        colors = [clickable.color for clickable in clickables]
        search_regions = [clickable.search_region for clickable in clickables]
        locs = self.locate(paths, scale=scales, uiscale=uiscales, timeout=0, threshold=thresholds, color=colors, search_region=search_regions)
        for clickable, loc in zip(clickables, locs):
            if clickable.click and loc and clickable.index < len(loc):
                if clickable.pre_click_callback is not None:
                    clickable.pre_click_callback()
                loc = loc[:min(len(loc), clickable.count)] # keep at most count loc
                if clickable.sort == Clickable.SortOrder.LEFT_TO_RIGHT_TOP_TO_BOTTOM:
                    loc = sorted(loc, key=lambda l: (l.x, l.y)) # sort left to right, top to bottom
                elif clickable.sort == Clickable.SortOrder.TOP_TO_BOTTOM_LEFT_TO_RIGHT:
                    loc = sorted(loc, key=lambda l: (l.y, l.x)) # sort top to bottom, left to right
                if clickable.message:
                    self.log(clickable.message)
                if clickable.click_callback:
                    clickable.click_callback(loc[clickable.index])
                else:
                    self.mouse_click(loc[clickable.index])
                sleep(0.1)
                if clickable.post_click_callback is not None:
                    result = clickable.post_click_callback()
                    if result is not None:
                        return result
                return clickable.result
        return False

    def __get_ffxi_settings(self):
        settings = self.settings.get()
        self.screen_res_w = None
        self.screen_res_h = None
        self.menu_res_w = None
        self.menu_res_h = None
        self.uiscale = 1.0
        if 'width' in settings and 'height' in settings:
            self.screen_res_w = int(settings.width)
            self.screen_res_h = int(settings.height)
            if 'uiscale' in settings:
                self.uiscale = float(settings.uiscale)
                self.menu_res_w = int(round(self.screen_res_w / self.uiscale))
                self.menu_res_h = int(round(self.screen_res_h / self.uiscale))

    def __update_ffxi_settings(self):
        # set windower profile resolution and ui scale in registry to enable different ffxi settings in pol clients from the same region
        self.__get_ffxi_settings()
        settings = self.settings.get()
        if 'region' in settings:
            region = settings.region
            if not self.ashita and Window.enable_multiple_windower_profiles:
                # windower sets these registry settings when it launches pol, but they need to be overwritten when ffxi starts to enable support for different windower profiles
                Window.__set_ffxi_registry_settings(region, self.screen_res_w, self.screen_res_h, self.menu_res_w, self.menu_res_h)

    def __pol_wait_for_ffxi(self):
        # 1. wait for pol window to disappear
        # 2. wait for ffxi window to appear
        # 3. click accept
        # self.log('Waiting for pol window to disappear')
        Window.waiting_for_ffxi = True
        self.activate()

        clickables = [
            Clickable(
                message='Connection error, retrying',
                path=Window.get_image_path('network_communication_error_retry.png'),
                scale=self.dpi_scale,
                result=False,
                pre_click_callback=None,
                post_click_callback=None,
                search_region=[328, 408, 500, 444]
                ),
            Clickable(
                message='Connection error, reconnecting',
                path=Window.get_image_path('network_connection_error_exit.png'),
                scale=self.dpi_scale,
                result=True,
                pre_click_callback=None,
                post_click_callback=partial(self.set_step, 1),
                search_region=[472, 400, 600, 456]
                ),
            ]

        # 1. wait for pol window to disappear
        while self.window is None or self.window.title.lower() != Window.final_fantasy_window_title.lower():
            self.activate(timeout=2)
            self.__update_window(timeout=ffxi_launch_timeout)
            if self.__click_list(clickables):
                return False
            sleep(0.2)
        self.log('pol window no longer visible')

        # 2. wait for ffxi window to appear
        start = time()
        while (time() - start) < ffxi_launch_timeout:
            # wait for ffxi window to appear and fully initialize
            self.__update_window(timeout=ffxi_launch_timeout)
            if self.window is None:
                continue
            if win32gui.IsWindowVisible(self.window.handle) and self.__get_ffxi_scale() is not None:
                self.activate()
                clickables = [
                    # 3. click accept
                    Clickable(
                        message=None,
                        path=Window.get_image_path('8_ffxi_login.png'),
                        scale=self.__get_ffxi_scale,
                        uiscale=self.__get_ffxi_uiscale,
                        threshold=0.8,
                        result=True,
                        pre_click_callback=None,
                        post_click_callback=partial(self.log, 'Click "accept"'),
                        search_region=[1700, 1000, 2300, 1300]
                    )
                ]

                if self.__click_list(clickables):
                    Window.waiting_for_ffxi = False
                    return True
            sleep(0.2)

        return False

    def __locate_failed(self):
        self.log('Unable to locate region to click')

    @staticmethod
    def get_image_path(path):
        return os.path.join(Window.image_path, path)

    def __get_next_available(self):
        unclaimed_windows = Window.__get_unclaimed_windows(Window.pol_window_title)
        unclaimed_windows = sorted(unclaimed_windows, key=lambda w: get_process_creation_time(w.pid)) # sort unclaimed windows in increasing order of process creation time
        if len(unclaimed_windows) > 0 and 'Not Responding' not in unclaimed_windows[0].title:
            Window.claimed_windows += [unclaimed_windows[0]]
            return unclaimed_windows[0]
        else:
            return None

    @staticmethod
    def __get_unclaimed_windows(title):
        window_list = win_list(title)
        claimed_pids = [window.pid for window in Window.claimed_windows]
        unclaimed = [idx for idx, window in enumerate(window_list) if window.pid not in claimed_pids]
        return [window_list[idx] for idx in unclaimed]

    @staticmethod
    def __get_slot():
        Window.slot_count += 1
        return Window.slot_count

    @staticmethod
    def __set_ffxi_registry_settings(region, screen_res_w, screen_res_h, menu_res_w, menu_res_h):
        #hex(x)
        reg_path = r'SOFTWARE\WOW6432Node\PlayOnline' + f'{region}' + r'\SquareEnix\FinalFantasyXI'
        reg_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, reg_path, 0, winreg.KEY_SET_VALUE)
        # reg_value = winreg.QueryValueEx(reg_key, '0001')[0]
        if screen_res_w is not None and screen_res_h is not None:
            winreg.SetValueEx(reg_key, '0001', 0, winreg.REG_DWORD, screen_res_w)
            winreg.SetValueEx(reg_key, '0002', 0, winreg.REG_DWORD, screen_res_h)
            winreg.SetValueEx(reg_key, '0003', 0, winreg.REG_DWORD, screen_res_w)
            winreg.SetValueEx(reg_key, '0004', 0, winreg.REG_DWORD, screen_res_h)
        if menu_res_w is not None and menu_res_h is not None:
            winreg.SetValueEx(reg_key, '0037', 0, winreg.REG_DWORD, menu_res_w)
            winreg.SetValueEx(reg_key, '0038', 0, winreg.REG_DWORD, menu_res_h)
        winreg.CloseKey(reg_key)


class Settings:

    @abstractmethod
    def get(self) -> EasyDict:
        ...

class WindowerSettings(Settings):

    def __init__(self, windower_path, profile_name):
        settings_filename = windower_path / 'settings.xml'
        self.root = ET.parse(settings_filename).getroot()
        self.settings = self.__init_settings(profile_name)

    def __init_settings(self, profile_name):
        for element in self.root.findall('profile'):
            name = element.attrib['name']
            if name == profile_name:
                profile_dict = EasyDict()
                iter = element.iter()
                next(iter)
                for child in iter:
                    profile_dict[child.tag] = child.text
                return profile_dict
        raise RuntimeError(f'Windnower profile named "{profile_name}" does not exist in windower settings.xml')

    def get(self):
        return self.settings

class AshitaSettings(Settings):

    def __init__(self, ashita_path, profile_name):
        settings_filename = ashita_path / f'config/boot/{profile_name}.ini'
        self.config = configparser.ConfigParser()
        self.config.read(settings_filename)
        self.settings = self.__init_settings()

    def __init_settings(self):
        window_width = None
        try:
            window_width = int(AshitaSettings.trim_comment(self.config['ffxi.registry']['0001']))
        except:
            ...
        window_height = None
        try:
            window_height = int(AshitaSettings.trim_comment(self.config['ffxi.registry']['0002']))
        except:
            ...
        menu_width = None
        try:
            menu_width = int(AshitaSettings.trim_comment(self.config['ffxi.registry']['0037']))
        except:
            ...
        menu_height = None
        try:
            menu_height = int(AshitaSettings.trim_comment(self.config['ffxi.registry']['0038']))
        except:
            ...
        uiscale = float(window_width) / float(menu_width)
        region = None
        try:
            region = int(AshitaSettings.trim_comment(self.config['ashita.language']['playonline']))
        except:
            ...
        if region == 1:
            region = 'JP'
        elif region == 2:
            region = 'US'
        elif region == 3:
            region = 'EU'
        else:
            region = 'US'
        return EasyDict(
            width=window_width,
            height=window_height,
            uiscale=uiscale,
            region=region,
        )

    @staticmethod
    def trim_comment(line):
        return line.split(';')[0].rstrip()

    def get(self):
        return self.settings

class VirtualKeyboard():

    def __init__(self,
                 image_path=data_dir / 'images' / 'pol_keyboard.png',
                 sequence_path=None
                 ):
        if not sequence_path:
            sequence_path = input('Enter a filename for keyboard sequence [default: pol_keyboard_sequence]: ')
            if not sequence_path:
                sequence_path = 'pol_keyboard_sequence'
            if Path(sequence_path).exists():
                result = input(f'Overwrite existing keyboard click sequence {sequence_path}? (y/n) [default: n]: ')
                if result.lower() != 'y':
                    sys.exit(0)
        self.sequence_path = sequence_path
        self.keyboard_im_rgb = Image.open(image_path)
        self.keyboard_im = np.asarray(self.keyboard_im_rgb.convert('L'))
        self.x0 = 8
        self.y0 = 31
        self.w = 20
        self.h = 22
        self.wgap = 2
        self.rect = [EasyDict(x=120, y=170, w=self.w, h=self.h)]
        for i in range(19):
            x = 120 + i * (self.w + self.wgap)
            y = 199
            self.rect += [EasyDict(x=x, y=y, w=self.w, h=self.h)]
            y = 229
            self.rect += [EasyDict(x=x, y=y, w=self.w, h=self.h)]
            y = 253
            self.rect += [EasyDict(x=x, y=y, w=self.w, h=self.h)]
            y = 283
            self.rect += [EasyDict(x=x, y=y, w=self.w, h=self.h)]
            y = 307
            self.rect += [EasyDict(x=x, y=y, w=self.w, h=self.h)]
        self.rect = sorted(self.rect, key = lambda item: (item.y, item.x))

    def get_locations(self):
        return [EasyDict(x=int(round(self.rect[self.sequence[i]].x+self.rect[self.sequence[i]].w/2)), y=int(round(self.rect[self.sequence[i]].y+self.rect[self.sequence[i]].h/2))) for i in range(self.length())]

    def get_templates(self):
        return [self.get_template(i) for i in range(self.length())]

    def get_template(self, index):
        return self.__crop_key(self.rect[self.sequence[index]])

    def __crop_key(self, rect):
        return self.keyboard_im[rect.y:rect.y+rect.h, rect.x:rect.x+rect.w]

    def length(self):
        return len(self.sequence)

    def __render_index(self):
        index_array = np.asarray(self.keyboard_im_rgb.copy())
        for rect in self.rect:
            index_array = cv2.rectangle(index_array, (rect.x - self.x0, rect.y - self.y0), (rect.x + rect.w - self.x0, rect.y + rect.h - self.y0), (0, 0, 255), 2)
        return Image.fromarray(index_array)

    def from_user(self):
        self.sequence = self.__get_user_clicks()

    def save(self):
        if len(self.sequence) <= 1:
            warnings.warn('Keyboard sequence is empty and will not be saved.')
            return
        if Path(self.sequence_path).is_file():
            warnings.warn('Overwriting keyboard sequence file')
        with open(self.sequence_path, 'wb') as fp:
            key, salt, password_enabled = self.__input_password('Enter password for keyboard sequence to be saved (press enter for no password): ')
            f = Fernet(key)
            sequence_encrypted = f.encrypt(str(self.sequence).encode())
            fp.write((b'1' if password_enabled else b'0') + salt + base64.urlsafe_b64decode(sequence_encrypted))

    def load(self):
        assert self.sequence_path, 'sequence_path is empty or None'
        assert Path(self.sequence_path).is_file(), f'{self.sequence_path} file does not exist'
        with open(self.sequence_path, 'rb') as fp:
            salt_and_sequence = fp.read()
            password_enabled = bool(int(bytes([salt_and_sequence[0]])))
            salt = salt_and_sequence[1:33]
            sequence_encrypted = base64.urlsafe_b64encode(salt_and_sequence[33:])
            sequence_string = None
            while not sequence_string:
                if password_enabled:
                    key, _, _ = self.__input_password('Enter password for keyboard sequence to be loaded:', salt=salt)
                else:
                    key, _, _ = self.__input_password(salt=salt)
                f = Fernet(key)
                try:
                    sequence_string = f.decrypt(sequence_encrypted).decode()
                except InvalidToken:
                    print('Invalid password.  Please try again.')
                    continue
            self.sequence = [int(x) for x in sequence_string.strip('][').split(', ')]

    def __input_password(self, prompt=None, salt=None):
        if prompt:
            # password_provided = "password" # use hard-coded password for debugging
            password_provided = getpass.getpass(prompt=prompt)  # prompt user for password
        else:
            password_provided = ''
        password_enabled = len(password_provided) > 0
        password = password_provided.encode() # convert to bytes
        if not salt:
            salt = os.urandom(32)
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(password))
        return key, salt, password_enabled

    def __get_user_clicks(self):
        # init tkinter
        self.tkroot = tk.Tk()

        # load image
        image = self.__render_index()
        photo = ImageTk.PhotoImage(image)

        # label with image
        l = tk.Label(self.tkroot, image=photo)
        l.pack()

        # bind click event
        sequence = []
        l.bind('<Button-1>', partial(self.__on_click, sequence))

        # bind window "X" close event
        self.tkroot.protocol('WM_DELETE_WINDOW', partial(self.__on_close, sequence))

        # reset the sequence and open the window for user to click keys
        self.tkroot.mainloop()

        return sequence

    def __on_close(self, sequence):
        '''
        Abort sequence capture.
        Called when the user clicks the "X" button at the top righthand corner of the window.
        '''
        sequence = [] # reset the sequence to prevent saving an incomplete sequence
        self.tkroot.destroy()

    def __on_click(self, sequence, event=None):
        x = event.x + self.x0
        y = event.y + self.y0

        # determine the index corresponding to the key that the user clicked
        for i, rect in enumerate(self.rect):
            # check if the click is within the bounds of this rectangle
            if x >= rect.x and x <= (rect.x + rect.w) and y >= rect.y and y <= (rect.y + rect.h):
                sequence += [i]
                if i == 0: # if the user clicks the circle, we're done so close the window
                    self.tkroot.destroy()
                else: #otherwise, break the loop to continue collecting user clicks
                    break


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--keyboard", help="capture from user, encrypt/password protect, and save pol virtual keyboard click sequence", action="store_true")

    return parser.parse_args()


def main():
    args = parse_args()

    if args.keyboard:
        keyboard = VirtualKeyboard()
        keyboard.from_user()
        keyboard.save()


if __name__== "__main__":
    main()
