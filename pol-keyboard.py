import sys
import os
from pathlib import Path
os.chdir(Path(__file__).parent)
sys.path.remove(os.getcwd())
from ffxi.pol import main


if __name__== '__main__':
    sys.argv += ['--keyboard']
    main()
